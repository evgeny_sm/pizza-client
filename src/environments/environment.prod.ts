export const environment = {
    production: true,
    BASE_URL: 'http://' + window.location.host,
    BASE_API_URL: 'http://' + window.location.host + '/api'
};
