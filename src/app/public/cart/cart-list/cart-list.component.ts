import {Component, EventEmitter, Input, Output, OnInit} from '@angular/core';
import {BasketItem} from '../../../shared/basket-item';

@Component({
    selector: 'app-cart-list',
    templateUrl: './cart-list.component.html',
    styleUrls: ['./cart-list.component.css']
})
export class CartListComponent implements OnInit {
    @Input() items: BasketItem[] = [];
    @Output() increase = new EventEmitter<BasketItem>();
    @Output() decrease = new EventEmitter<BasketItem>();
    @Output() remove = new EventEmitter<BasketItem>();

    constructor() {
    }

    ngOnInit() {
    }

    getSum() {
        let result = 0;
        for (const item of this.items) {
            result += item.count * item.item.price;
        }
        return result;
    }
}
