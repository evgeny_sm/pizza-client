import {Component, OnInit, OnDestroy} from '@angular/core';
import {Subscription} from 'rxjs/Subscription';

import {BasketService} from '../../shared/basket.service';
import {BasketItem} from '../../shared/basket-item';
import {OrderService} from '../../shared/order.service';


@Component({
    selector: 'app-cart',
    templateUrl: './cart.component.html',
    styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit, OnDestroy  {
    public order?: any = null;
    public items: BasketItem[] = [];
    public basketSubscription?: Subscription;
    constructor(private basketService: BasketService, private orderService: OrderService) {
    }

    ngOnInit() {
        this.basketSubscription = this.basketService.getList().subscribe(
            data => {
                this.items = data;
            },
            error => console.log(error)
        );
    }

    ngOnDestroy() {
        this.basketSubscription.unsubscribe();
    }

    increase(item: BasketItem) {
        this.basketService.changeItemCount(item.item, item.count + 1);
    }

    decrease(item: BasketItem) {
        this.basketService.changeItemCount(item.item, item.count - 1);
    }

    remove(item: BasketItem) {
        this.basketService.changeItemCount(item.item, 0);
    }

    addOrder(values: any) {
        const items = {};
        this.basketService.getList().subscribe((result: BasketItem[]) => {
            for(const item of result) {
                items[item.item.id] = item.count;
            }
        });
        this.orderService.add({form: values, items}).subscribe(data => {
            this.basketService.removeAll();
            this.order = data;
        });
    }
}
