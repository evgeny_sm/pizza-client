import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';

import {emailOrEmptyValidator} from '../../../shared/email-or-empty.validator';

@Component({
    selector: 'app-cart-order-form',
    templateUrl: './cart-order-form.component.html',
    styleUrls: ['./cart-order-form.component.css']
})
export class CartOrderFormComponent implements OnInit {
    @Output() addOrder = new EventEmitter<any>();
    isSendTouched = false;
    orderForm: FormGroup;
    street: FormControl;
    house: FormControl;
    apart: FormControl;
    name: FormControl;
    phone: FormControl;
    email: FormControl;
    note: FormControl;
    phoneMask = ['\+', '7', ' ', '(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, '-', /\d/, /\d/];

    constructor() {
    }

    ngOnInit() {
        this.createFormControls();
        this.createForm();
    }

    createForm() {
        this.orderForm = new FormGroup({
            address: new FormGroup({
                street: this.street,
                house: this.house,
                apart: this.apart,
            }),
            name: this.name,
            phone: this.phone,
            email: this.email,
            note: this.note,
        });
    }

    createFormControls() {
        this.street = new FormControl('', [Validators.required, Validators.minLength(2)]);
        this.house = new FormControl('', Validators.required);
        this.apart = new FormControl('', Validators.required);
        this.name = new FormControl('', [Validators.required, Validators.minLength(2)]);
        this.phone = new FormControl('', [Validators.required, Validators.minLength(18)]);
        this.email = new FormControl('', emailOrEmptyValidator);
        this.note = new FormControl('');
    }

    onSubmit() {
        this.isSendTouched = true;
        if(!this.orderForm.valid) return;

        this.addOrder.emit(this.orderForm.value);
    }

}
