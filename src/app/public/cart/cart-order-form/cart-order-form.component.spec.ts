import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CartOrderFormComponent } from './cart-order-form.component';

describe('CartOrderFormComponent', () => {
  let component: CartOrderFormComponent;
  let fixture: ComponentFixture<CartOrderFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CartOrderFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CartOrderFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
