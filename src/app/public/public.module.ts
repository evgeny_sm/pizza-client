import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ReactiveFormsModule} from '@angular/forms';
import {TextMaskModule} from 'angular2-text-mask';

import {ShowcaseComponent} from './showcase/showcase.component';
import {CatalogService} from '../shared/catalog.service';
import {SharedModule} from '../shared/shared.module';
import {OrderService} from '../shared/order.service';
import {CartOrderFormComponent} from './cart/cart-order-form/cart-order-form.component';
import {CartListComponent} from './cart/cart-list/cart-list.component';
import {CartComponent} from './cart/cart.component';
import {OrderComponent} from './order/order.component';
import {OrderViewComponent} from './order/order-view/order-view.component';
import {OrderItemViewComponent} from './order/order-item-view/order-item-view.component';
import {RouterModule} from '@angular/router';

@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        ReactiveFormsModule,
        TextMaskModule,
        RouterModule
    ],
    declarations: [ShowcaseComponent, CartComponent, CartListComponent, CartOrderFormComponent,
        OrderViewComponent, OrderComponent, OrderItemViewComponent],
    providers: [CatalogService, OrderService],
})
export class PublicModule {
}
