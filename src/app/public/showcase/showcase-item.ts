import {CatalogItem} from '../../shared/catalog-item';

export class ShowcaseItem extends CatalogItem {
    public countInBasket = 0;
    constructor(oData, countInBasket = 0) {
        super(oData);
        this.countInBasket = countInBasket;
    }
}
