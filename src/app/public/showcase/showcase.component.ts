import {Component, DoCheck, OnDestroy, OnInit} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/combineLatest';

import {CatalogService} from '../../shared/catalog.service';
import {CatalogItem} from '../../shared/catalog-item';
import {BasketService} from '../../shared/basket.service';
import {Subscription} from 'rxjs/Subscription';
import {BasketItem} from '../../shared/basket-item';
import {ShowcaseItem} from './showcase-item';

@Component({
    selector: 'app-showcase',
    templateUrl: './showcase.component.html',
    styleUrls: ['./showcase.component.css'],
})
export class ShowcaseComponent implements OnInit, OnDestroy {
    public list?: ShowcaseItem[] = null;

    private combinedSubscription: Subscription;

    constructor(private catalogService: CatalogService,
                private basketService: BasketService) {
    }

    ngOnInit() {
        this.combinedSubscription = Observable.combineLatest(
            this.catalogService.getList(),
            this.basketService.getList(),
            (catalogData: any, basket: any) => {
                const catalog: CatalogItem[] = catalogData.rows;
                const result = [];
                catalog.forEach((item) => {
                    const basketItem = basket.find((element: BasketItem) => {
                        return element.item.id === item.id;
                    });
                    result.push(new ShowcaseItem(item, basketItem ? basketItem.count : 0));
                });
                return result;
            }
        ).subscribe(
            (data: ShowcaseItem[]) => this.list = data,
            error => console.log(error)
        );
    }

    ngOnDestroy() {
        this.combinedSubscription.unsubscribe();
    }

    addItem(item: ShowcaseItem) {
        item.countInBasket++;
        this.basketService.changeItemCount(item, item.countInBasket);
    }

    subItem(item: ShowcaseItem) {
        item.countInBasket--;
        this.basketService.changeItemCount(item, item.countInBasket);
    }

    setItemCount(item: ShowcaseItem, value: number) {
        value = +value;
        value = value > 0 ? value : 0;
        this.basketService.changeItemCount(item, value);
    }
}
