import {Component, Input, OnInit} from '@angular/core';

@Component({
    selector: 'app-order-view',
    templateUrl: './order-view.component.html',
    styleUrls: ['./order-view.component.css']
})
export class OrderViewComponent implements OnInit {
    @Input() order: any = {};

    constructor() {}

    ngOnInit() {

    }

}
