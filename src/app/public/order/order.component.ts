import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Params} from '@angular/router';
import {OrderService} from '../../shared/order.service';

@Component({
    selector: 'app-order',
    templateUrl: './order.component.html',
    styleUrls: ['./order.component.css']
})
export class OrderComponent implements OnInit {
    order?: any = null;

    constructor(private orderService: OrderService,
                private activatedRoute: ActivatedRoute) {
    }

    ngOnInit() {
        this.activatedRoute.params.forEach((params: Params) => {
            const id = +params['id'];
            const token = params['token'];
            this.orderService.getByIdAndToken(id, token).subscribe(
                (order: any) => {
                    this.order = order;
                },
                error => {
                    console.log('error', error);
                }
            );
        });
    }

}
