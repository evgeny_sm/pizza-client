import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {ShowcaseComponent} from './showcase/showcase.component';
import {CartComponent} from './cart/cart.component';
import {OrderComponent} from './order/order.component';

const routes: Routes = [
    {path: 'catalog', component: ShowcaseComponent, data: {title: 'Каталог'}},
    {path: 'cart', component: CartComponent, data: {title: 'Корзина'}},
    {path: 'order/:id/:token', component: OrderComponent, data: {title: 'Просмотр заказа'}},
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PublicRoutingModule {
}
