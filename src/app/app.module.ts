import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {SharedModule} from './shared/shared.module';
import {BasketService} from './shared/basket.service';
import {AuthService} from './shared/auth.service';
import {HttpModule} from '@angular/http';
import {AuthGuard} from './shared/auth-guard.service';
import {StorageFileService} from './shared/storage-file.service';
import {PublicModule} from './public/public.module';

@NgModule({
    declarations: [
        AppComponent,
    ],
    imports: [
        BrowserModule,
        NgbModule.forRoot(),
        AppRoutingModule,
        HttpModule,
        SharedModule,
        PublicModule
    ],
    bootstrap: [AppComponent],
    providers: [StorageFileService, AuthGuard, AuthService, BasketService]
})
export class AppModule {
}
