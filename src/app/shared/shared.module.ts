import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {IntegerDirective} from './integer.directive';
import {RuPluralPipe} from './ru-plural.pipe';
import {NotFoundComponent} from './not-found/not-found.component';
import {RouterModule} from '@angular/router';
import {MiniBasketComponent} from './mini-basket/mini-basket.component';
import {LoginComponent} from './login/login.component';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {BreadCrumbsComponent} from './bread-crumbs/bread-crumbs.component';
import {PaginationComponent} from './pagination/pagination.component';

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        HttpModule,
        FormsModule
    ],
    declarations: [
        IntegerDirective,
        RuPluralPipe,
        NotFoundComponent,
        MiniBasketComponent,
        LoginComponent,
        BreadCrumbsComponent,
        PaginationComponent
    ],
    exports: [
        IntegerDirective,
        RuPluralPipe,
        NotFoundComponent,
        MiniBasketComponent,
        LoginComponent,
        BreadCrumbsComponent,
        PaginationComponent
    ],
})
export class SharedModule {
}
