import {CatalogItem} from './catalog-item';
export class BasketItem {
    public item: CatalogItem;
    public count: number;

    constructor(item: CatalogItem, count: number) {
        this.item = item;
        this.count = count;
    }
}
