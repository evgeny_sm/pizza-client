import {Injectable} from '@angular/core';
import {Http, RequestOptions, Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import {environment} from '../../environments/environment';
import {CatalogItem} from './catalog-item';

@Injectable()
export class CatalogService {
    private url: string;
    private options: RequestOptions = new RequestOptions();

    constructor(public http: Http) {
        this.url = environment.BASE_API_URL + '/catalog';
        this.options.withCredentials = true;
    }

    public getList(paging?: any): Observable<CatalogItem[]> {
        let request = this.options;
        if(paging) {
            request = Object.assign({}, request, {params: paging});
        }
        return this.http.get(this.url, request)
            .map((resp: Response) => {
                const list = [];
                const response = resp.json();
                for (let i = 0; i < response.rows.length; i++) {
                    list.push(new CatalogItem(response.rows[i]));
                }
                return {rows: list, count: response.count};
            })
            .catch((error: any) => {
                return Observable.throw(error);
            });
    }

    public getItem(id: any): Observable<CatalogItem> {
        return this.http.get(this.url + '/' + id, this.options)
            .map((resp: Response) => {
                return new CatalogItem(resp.json());
            })
            .catch((error: any) => {
                console.log(error);
                return Observable.throw(error);
            });
    }

    public addItem(item: CatalogItem): Observable<CatalogItem> {
        return this.http.post(this.url, {item: item}, this.options)
            .map((resp: Response) => {
                return new CatalogItem(resp.json());
            })
            .catch((error: any) => {
                console.log(error);
                return Observable.throw(error);
            });
    }

    public saveItem(item: CatalogItem): Observable<CatalogItem> {
        return this.http.put(this.url + '/' + item.id, {item: item}, this.options)
            .map((resp: Response) => {
                return new CatalogItem(resp.json());
            })
            .catch((error: any) => {
                console.log(error);
                return Observable.throw(error);
            });
    }

    public deleteItem(id: number): Observable<boolean> {
        return this.http.delete(this.url + '/' + id, this.options)
            .map((resp: Response) => {
                return true;
            })
            .catch((error: any) => {
                console.log(error);
                return Observable.throw(error);
            });
    }
}
