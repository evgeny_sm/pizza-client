import {Component, OnInit} from '@angular/core';
import {AuthService} from '../auth.service';
import {Router} from '@angular/router';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
    public userLogin: string;
    public userPassword: string;
    public hasError = false;

    constructor(public authService: AuthService, public router: Router) {
    }

    ngOnInit() {
    }

    login() {
        this.authService.login(this.userLogin, this.userPassword).subscribe(
            isSuccess => {
                if(this.authService.user) {
                    this.router.navigate([this.authService.redirectUrl]);
                } else {
                    this.hasError = true;
                }
            },
            error => {
                this.hasError = true;
            }
        );
    }

    logout() {
        this.authService.logout()
            .subscribe(() => {
            console.log('logout');
        });
    }

}
