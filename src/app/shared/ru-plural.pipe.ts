import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
    name: 'ruPlural'
})
export class RuPluralPipe implements PipeTransform {

    transform(value: number, forms: string[]): any {
        if(value == null) return '';

        if (forms.length === 2)
            forms.push(forms[1]);

        const intNum = Math.abs(value) % 100;
        const n1 = intNum % 10;
        if(intNum > 10 && intNum < 20) {
            return forms[2];
        } else if(n1 > 1 && n1 < 5) {
            return forms[1];
        } else if(n1 === 1) {
            return forms[0];
        }
        return forms[2];
    }

}
