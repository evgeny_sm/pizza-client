import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {AuthService} from './auth.service';

@Injectable()
export class AuthGuard implements CanActivate {

    constructor(private authService: AuthService, private router: Router) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        return this.authService.loadPromise.then(
            () => {
                return this.checkAuth(route, state);
            },
            () => {
                return false;
            }
        );
    }

    checkAuth(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        if(this.authService.user) {
            if(typeof route.data !== 'undefined' && typeof route.data.access !== 'undefined') {
                if(!this.authService.hasAccess(route.data.access)) {
                    this.authService.redirectUrl = state.url;
                    this.router.navigate(['/login']);
                    return false;
                }
            }
            return true;
        } else {
            this.authService.redirectUrl = state.url;
            this.router.navigate(['/login']);
            return false;
        }
    }

}
