import {Directive, ElementRef, HostListener} from '@angular/core';

@Directive({
    selector: '[appInteger]'
})
export class IntegerDirective {

    constructor(private element: ElementRef) {}

    @HostListener('input') onInput() {
        if (isNaN(+this.element.nativeElement.value)) {
            this.element.nativeElement.value = 0;
        }
        this.element.nativeElement.value = Math.ceil(+this.element.nativeElement.value);
    }

}
