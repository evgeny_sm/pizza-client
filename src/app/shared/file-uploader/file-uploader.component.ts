import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {StorageFile} from '../storage-file';
import {StorageFileService} from '../storage-file.service';

@Component({
    selector: 'app-file-uploader',
    templateUrl: './file-uploader.component.html',
    styleUrls: ['./file-uploader.component.css']
})
export class FileUploaderComponent implements OnInit {
    @Input() file?: any;
    @Output() fileChanged = new EventEmitter<StorageFile>();
    @Output() fileRemoved = new EventEmitter();

    constructor(private storageFileService: StorageFileService) {
    }

    ngOnInit() {
        if(typeof this.file === 'object') {
            this.file = this.file;
        } else if(+this.file > 0) {
            this.storageFileService.getFile(this.file)
                .subscribe((file: StorageFile) => {
                    this.file = file;
                });
        }
    }

    fileChange(event) {
        const fileList: FileList = event.target.files;
        if(fileList.length > 0) {
            const file: File = fileList[0];
            this.storageFileService.add(file)
                .subscribe((newfile: StorageFile) => {
                    this.file = newfile;
                    this.fileChanged.emit(this.file);
                });
        }
    }

    removeFile() {
        this.fileRemoved.emit();
    }

    isPicture() {
        return this.file.mimetype.indexOf('image/') >= 0;
    }
}
