import {Injectable} from '@angular/core';
import {Http, RequestOptions, Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import {environment} from '../../environments/environment';


@Injectable()
export class AuthService {

    private url: string;
    private options: RequestOptions = new RequestOptions();

    public redirectUrl = '/';
    public user?: any = null;
    public loadPromise: Promise<boolean>;

    constructor(private http: Http) {
        this.url = environment.BASE_API_URL + '/auth';
        this.options.withCredentials = true;

        this.loadPromise = new Promise<boolean>((resolve, reject) => {
            this.loadInfo()
                .subscribe((user: any) => {
                    this.user = user;
                    return resolve(true);
                },
                error => {
                     console.log(error);
                     return reject(true);
                });
        }).catch(() => {
            return false;
        });
    }

    loadInfo(): Observable<any> {
        return this.http.get(this.url + '/info', this.options)
            .catch(error => {
                return Promise.reject('unauthorized');
            })
            .map((res: Response) => {
                return res.json();
            });
    }

    login(login: string, password: string): Observable<boolean> {
        return this.http.post(this.url + '/login', {login: login, password: password}, this.options)
            .map((res: Response) => {
                this.user = res.json();
                return true;
            })
            .catch((err: any) => {
                return Observable.throw(err);
            });
    }
    logout(): Observable<boolean> {
        return this.http.get(this.url + '/logout', this.options)
            .map((res: Response) => {
                this.user = null;
                return true;
            })
            .catch((err: any) => {
                return Observable.throw(err);
            });
    }

    hasAccess(requiredAccess: string): boolean {
        if(!this.user) return false;

        for(const group of this.user.userGroups) {
            for(const oAcces of group.userGroupAccesses) {
                if(oAcces.code === requiredAccess) {
                    return true;
                }
            }
        }
        return false;
    }

}
