import {environment} from '../../environments/environment';
export class StorageFile {
    public id: number;
    public name: string;
    public path: string;
    public size: number;
    public mimetype: string;
    constructor(data) {
        Object.assign(this, data);
    }
    get url() {
        if(this.path.length > 0) {
            return environment.BASE_URL + '/' + this.path;
        } else {
            return '';
        }
    }
}
