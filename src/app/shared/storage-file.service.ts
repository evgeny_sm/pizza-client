import {Injectable} from '@angular/core';
import {Http, RequestOptions, Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {StorageFile} from '../shared/storage-file';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import {environment} from '../../environments/environment';

@Injectable()
export class StorageFileService {
    private url: string;
    private options: RequestOptions = new RequestOptions();

    constructor(public http: Http) {
        this.url = environment.BASE_API_URL + '/file';
        this.options.withCredentials = true;
    }

    getFile(id: number): Observable<StorageFile> {
        return this.http.get(this.url + '/' + id, this.options)
            .map((response: Response) => {
                return new StorageFile(response.json());
            });
    }

    add(file: File): Observable<StorageFile> {
        const formData: FormData = new FormData();
        formData.append('uploadFile', file, file.name);
        return this.http.post(this.url, formData, this.options)
            .map((response: Response) => {
                return new StorageFile(response.json());
            });
    }
}
