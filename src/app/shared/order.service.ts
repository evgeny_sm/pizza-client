import {Injectable} from '@angular/core';
import {Http, RequestOptions, Response} from '@angular/http';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable()
export class OrderService {
    private url: string;
    private options: RequestOptions = new RequestOptions();

    constructor(private http: Http) {
        this.url = environment.BASE_API_URL + '/order';
        this.options.withCredentials = true;
    }

    add(data: any): Observable<any> {
        return this.http.post(this.url, data, this.options)
        .map((res: Response) => {
                return res.json();
            });
    }

    getByIdAndToken(id: number, token: string): Observable<any> {
        return this.http.get(this.url + '/' + id + '/' + token, this.options)
            .map((res: Response) => {
                return res.json();
            });
    }

}
