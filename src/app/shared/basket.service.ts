import {Injectable} from '@angular/core';
import {Http, RequestOptions, Response} from '@angular/http';
import {ReplaySubject} from 'rxjs/ReplaySubject';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import {environment} from '../../environments/environment';
import {BasketItem} from './basket-item';
import {CatalogItem} from './catalog-item';


@Injectable()
export class BasketService {
    private aData: BasketItem[] = [];
    private url: string;
    private options: RequestOptions = new RequestOptions();
    private subject: ReplaySubject<BasketItem[]> = new ReplaySubject<BasketItem[]>(1);

    constructor(private http: Http) {
        this.url = environment.BASE_API_URL + '/mini-basket';
        this.options.withCredentials = true;
        this.loadList();
    }

    private loadList() {
        this.http.get(this.url, this.options).subscribe((response: Response) => {
            const list = response.json() || [];
            this.aData = [];
            for (const item of list) {
                this.aData.push(new BasketItem(new CatalogItem(item.item), item.count));
            }
            this.subject.next(this.aData);
        }, error => {
            this.subject.error(error);
        });
    }

    private addItem(item: BasketItem): Observable<boolean> {
        return this.http.post(this.url, {id: item.item.id, count: item.count}, this.options)
            .map((res: Response) => {
                return true;
            }
        );
    }

    private saveItem(item: BasketItem): Observable<boolean> {
        return this.http.put(this.url + '/' + item.item.id, {id: item.item.id, count: item.count}, this.options)
            .map((res: Response) => {
                return true;
            }
        );
    }

    private removeItem(item: BasketItem): Observable<boolean> {
        return this.http.delete(this.url + '/' + item.item.id, this.options)
            .map((res: Response) => {
                return true;
            }
        );
    }

    private findItem(item: CatalogItem): BasketItem {
        for (const basketItem of this.aData) {
            if (basketItem.item.id === item.id) {
                return basketItem;
            }
        }
        return null;
    }

    removeAll() {
        this.http.delete(this.url, this.options).subscribe(() => {
            this.loadList();
        });
    }

    getList(): Observable<BasketItem[]> {
        return this.subject.asObservable();
    }

    changeItemCount(item: CatalogItem, count: number) {
        const basketItem = new BasketItem(item, count);

        if(!this.findItem(item)) {
            // добавить товар
            return this.addItem(basketItem).subscribe(() => {
                this.loadList();
            });
        }

        if(basketItem.count <= 0) {
            // удалить товар
            return this.removeItem(basketItem).subscribe(() => {
                this.loadList();
            });
        }

        // изменить кол-во товара
        this.saveItem(basketItem).subscribe(() => {
            this.loadList();
        });
    }
}
