import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';
import 'rxjs/add/operator/filter';

@Component({
    selector: 'app-bread-crumbs',
    templateUrl: './bread-crumbs.component.html',
    styleUrls: ['./bread-crumbs.component.css']
})
export class BreadCrumbsComponent implements OnInit {
    public list = [];
    private lastChild: ActivatedRoute = null;

    constructor(private router: Router,
                private activatedRoute: ActivatedRoute) {
    }

    ngOnInit() {
        this.router.events
            .filter((event) => event instanceof NavigationEnd)
            .subscribe((event) => {
                this.list.splice(0);
                let route: ActivatedRoute = this.activatedRoute.firstChild;
                do {
                    if (route.snapshot.data['title']) {
                        this.list.push(route.snapshot.data['title']);
                    }
                    this.lastChild = route;
                } while (route = route.firstChild);
            });
    }

    goBack(to: number) {
        let path = '';
        for(let i = 0; i < (this.list.length - 1 - to); i++) {
            path += '../';
        }
        this.router.navigate([path], {relativeTo: this.lastChild});
    }

}
