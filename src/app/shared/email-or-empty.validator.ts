import {FormControl, Validators} from '@angular/forms';

export function emailOrEmptyValidator(control: FormControl) {
    return control.value === '' ? null : Validators.email(control);
}