import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Paging} from '../paging';

@Component({
    selector: 'app-pagination',
    templateUrl: './pagination.component.html',
    styles: []
})
export class PaginationComponent implements OnInit {
    @Output() change = new EventEmitter<any>();
    paging = new Paging(1, 10, 0);
    public pages = [];

    constructor() {
    }

    ngOnInit() {

    }

    get totalCount(): number { return this.paging.totalCount; }
    @Input('totalCount') set totalCount(value: number) {
        this.paging.totalCount = value;
        this.initPages();
    }

    initPages() {
        this.pages = [];
        if(this.paging.totalCount <= 0) return;

        const iPageCount = Math.ceil(this.paging.totalCount / this.paging.countPerPage);
        let rangeStart = 1;
        let rangeEnd = 5;
        if(this.paging.page >= 3) {
            rangeStart = this.paging.page - 2;
            rangeEnd = this.paging.page + 2;
        }
        rangeStart = rangeStart > 1 ? rangeStart : 1;
        rangeEnd = rangeEnd <= iPageCount ? rangeEnd : iPageCount;
        for(let i = rangeStart; i <= rangeEnd; i++) {
            this.pages.push(i);
        }
        if(iPageCount > 0 && this.paging.page > iPageCount) {
            this.paging.page = iPageCount;
            this.emitState();
        }
    }

    emitState() {
        this.change.emit(this.paging.getRequest());
    }

    prevPage() {
        this.paging.page--;
        this.emitState();
        this.initPages();
    }

    nextPage() {
        this.paging.page++;
        this.emitState();
        this.initPages();
    }

    setPage(num: number) {
        this.paging.page = num;
        this.emitState();
        this.initPages();
    }

}
