import {Component, OnInit} from '@angular/core';

import {BasketService} from '../basket.service';
import {BasketItem} from '../basket-item';

@Component({
    selector: 'app-mini-basket',
    templateUrl: './mini-basket.component.html',
    styleUrls: ['./mini-basket.component.css'],
})
export class MiniBasketComponent implements OnInit {
    public list: BasketItem[] = [];

    constructor(private basketService: BasketService) {
    }

    ngOnInit() {
        this.basketService.getList().subscribe(
            data => {
                this.list = data;
            },
            error => { console.log(error); }
        );
    }

    getSumm() {
        let sum = 0;
        for (const item of this.list) {
            sum += item.count * item.item.price;
        }
        return sum;
    }

}
