import {StorageFile} from './storage-file';

export class CatalogItem {
    id: number;
    name: string;
    price: number;
    picture: number;
    picture_detail: number;
    oPicture?: StorageFile = null;
    oPictureDetail?: StorageFile = null;
    description: string;
    constructor(oData) {
        if(oData.oPicture && typeof oData.oPicture === 'object') {
            oData.oPicture = new StorageFile(oData.oPicture);
        }
        if(oData.oPictureDetail && typeof oData.oPictureDetail === 'object') {
            oData.oPictureDetail = new StorageFile(oData.oPictureDetail);
        }
        Object.assign(this, oData);
    }
}
