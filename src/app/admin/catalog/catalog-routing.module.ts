import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ListComponent} from './list/list.component';
import {EditComponent} from './edit/edit.component';

const routes: Routes = [
    {
        path: '',  data: {title: 'Товары'}, children: [
        {path: '', component: ListComponent, data: {title: 'Список'}},
        {path: ':id', component: EditComponent, data: {title: 'Редактирование'}}
    ]
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CatalogRoutingModule {
}
