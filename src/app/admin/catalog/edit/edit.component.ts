import {Component, OnInit} from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import {CatalogItem} from '../../../shared/catalog-item';
import {CatalogService} from '../../../shared/catalog.service';

@Component({
    selector: 'app-edit',
    templateUrl: './edit.component.html',
    styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {
    public item?: CatalogItem = new CatalogItem({id: null});

    constructor(private catalogService: CatalogService,
                private router: Router,
                private activatedRoute: ActivatedRoute) {
    }

    ngOnInit() {
        this.activatedRoute.params.forEach((params: Params) => {
            const id = +params['id'];
            if(isNaN(id)) {
                this.item = new CatalogItem({id: null});
                return;
            }
            this.catalogService.getItem(id).subscribe(
                (item: CatalogItem) => {
                    this.item = item;
                },
                error => {
                    console.log('error', error);
                }
            );
        });
    }

    addItem() {
        this.catalogService.addItem(this.item)
            .subscribe((item: CatalogItem) => {
                this.router.navigate(['../', item.id], { relativeTo: this.activatedRoute });
            });
    }
    saveItem() {
        this.catalogService.saveItem(this.item)
            .subscribe((item: CatalogItem) => {
                this.item = item;
            });
    }

    deleteItem() {
        if(!confirm('Вы действительно хотите удалить данный элемент?')) return;

        this.catalogService.deleteItem(this.item.id)
            .subscribe((success: boolean) => {
                this.router.navigate(['../'], { relativeTo: this.activatedRoute });
            });
    }

}
