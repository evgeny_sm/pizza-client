import {Component, OnInit} from '@angular/core';
import {CatalogItem} from '../../../shared/catalog-item';
import {CatalogService} from '../../../shared/catalog.service';

@Component({
    selector: 'app-list',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.css'],
    providers: [CatalogService]
})
// @todo: сделать хост-компонент, который будет получать данные с сервреа и подключать компоненты списка и пагинации
// @todo: добавить сохранение страницы с которой перешли в редактирование
export class ListComponent implements OnInit {
    public list?: CatalogItem[] = null;
    public totalCount = 0;
    private pagingRequest: any = {
        limit: 10,
        offset: 0
    };

    constructor(private catalogService: CatalogService) {
    }

    ngOnInit() {
        this.load();
    }

    load() {
        this.catalogService.getList(this.pagingRequest).subscribe(
            (data: any) => {
                this.list = data.rows;
                this.totalCount = data.count;
            },
            error => console.log(error)
        );
    }

    changedPaging(data: any) {
        this.pagingRequest = data;
        this.load();
    }

}
