import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {CatalogRoutingModule} from './catalog-routing.module';
import {ListComponent} from './list/list.component';
import {EditComponent} from './edit/edit.component';
import {CatalogService} from '../../shared/catalog.service';
import {FormsModule} from '@angular/forms';
import {FileUploaderComponent} from '../../shared/file-uploader/file-uploader.component';
import {SharedModule} from '../../shared/shared.module';

@NgModule({
    imports: [
        CommonModule,
        CatalogRoutingModule,
        FormsModule,
        SharedModule
    ],
    declarations: [ListComponent, EditComponent, FileUploaderComponent],
    providers: [CatalogService]
})
export class CatalogModule {
}
