import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {AdminComponent} from './admin.component';
import {NotFoundComponent} from '../shared/not-found/not-found.component';
import {AuthGuard} from '../shared/auth-guard.service';

const routes: Routes = [
    {
        path: '',
        component: AdminComponent,
        canActivate: [AuthGuard],
        data: {title: 'Администрирование', access: 'catalog.edit'},
        children: [
            {path: '', component: NotFoundComponent},
            {path: 'catalog', loadChildren: 'app/admin/catalog/catalog.module#CatalogModule'},
        ]
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AdminRoutingModule {
}
