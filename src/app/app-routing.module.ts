import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {NotFoundComponent} from './shared/not-found/not-found.component';
import {LoginComponent} from './shared/login/login.component';
import {PublicRoutingModule} from './public/public-routing.module';

export const routes: Routes = [
    {path: '', redirectTo: 'catalog', pathMatch: 'full'}, // пути в PublicRoutingModule
    {path: 'admin', loadChildren: 'app/admin/admin.module#AdminModule'},
    {path: 'login', component: LoginComponent, data: {title: 'Авторизация'}},
    {path: '**', component: NotFoundComponent, data: {title: 'Страница не найдена'}}
];

@NgModule({
    imports: [RouterModule.forRoot(routes), PublicRoutingModule],
    exports: [RouterModule],
})
export class AppRoutingModule {
}
